export const ADD_TEAM_MEMBER = 'users/ADD_TEAM_MEMBER';
export const REMOVE_TEAM_MEMBER = 'users/REMOVE_TEAM_MEMBER';

export const addTeamMember = userData => ({
    type: ADD_TEAM_MEMBER,
    userData
});

export const removeTeamMember = userId => ({
    type: REMOVE_TEAM_MEMBER,
    userId
});