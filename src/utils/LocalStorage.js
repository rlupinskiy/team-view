class LocalStorage {
    static setKeyValue(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    static getKeyValue(key) {
        const value = localStorage.getItem(key);

        return value ? JSON.parse(value) : null;
    }
}

export default LocalStorage;
