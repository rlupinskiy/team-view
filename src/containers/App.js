import React, { PureComponent } from 'react';
import UserList from './UserList';
import UserForm from './UserForm';
import './App.css';

class App extends PureComponent {
    render() {
        return (
            <div className="app">
                <div className="app-title">Your team for this test</div>
                <div className="wrapper">
                    <UserForm />
                    <UserList />
                </div>
            </div>
        );
    }
}

export default App;
