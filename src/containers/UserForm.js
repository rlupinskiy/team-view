import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as usersActions from '../actions/users';
import { getUserList } from '../selectors/users';
import './UserForm.css';


class UserForm extends PureComponent {
    state = {
        addTeamMember: false
    };

    handleInputChange = event => {
        const target = event.target;
        const { value } = target;
        const { userList, addTeamMember } = this.props;
        const userData = userList.find(user => user.id === Number(value));

        addTeamMember(userData);
    };

    addTeamMember = () => {
        this.setState({
            addTeamMember: true
        });
    };

    render() {
        const { userList } = this.props;
        const { addTeamMember } = this.state;

        return (
            <React.Fragment>
                <form className="user-form">
                    {(!addTeamMember) ? (
                        <React.Fragment>
                            <div className="add-user-sign" onClick={this.addTeamMember}>
                                <div className="add-user-before" />
                                <div className="add-user-after" />
                            </div>
                            <div className="add-user-title">
                                <div>Add team member</div>
                                <div>to this test</div>
                            </div>
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            <div>
                                <select name="role" defaultValue="" onChange={this.handleInputChange}>
                                    {
                                        userList.map(user => {
                                            return <option key={user.id} value={user.id}>{user.username}</option>
                                        })
                                    }
                                </select>
                            </div>
                        </React.Fragment>
                    )}
                </form>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    userList: getUserList(state)
});

const mapDispatchToProps = dispatch => ({
    addTeamMember: userData => {
        dispatch(usersActions.addTeamMember(userData))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);