import usersReducer, { initialState } from './users';
import * as usersActions from '../../actions/users';

describe('users reducer', () => {
    it('should return initial state', () => {
        const state = usersReducer(initialState, {});

        expect(state).toEqual(initialState);
    });

    it(`should handle ${usersActions.ADD_TEAM_MEMBER}`, () => {
        const teamMember = {
            id: 11,
            username: 'Test User'
        };
        const state = usersReducer(initialState, usersActions.addTeamMember(teamMember));
        const expectedResult = [...initialState.teamMembers, teamMember];

        expect(state.teamMembers).toEqual(expectedResult);
    });

    it(`should handle ${usersActions.REMOVE_TEAM_MEMBER}`, () => {
        const teamMembers = [{
            id: 1,
            username: 'Test User'
        }, {
            id: 2,
            username: 'Test User 2'
        }];
        const state = usersReducer({ teamMembers }, usersActions.removeTeamMember(1));
        const expectedResult = [{
            id: 2,
            username: 'Test User 2'
        }];

        expect(state.teamMembers).toEqual(expectedResult);
    });
});