import _ from 'lodash';
import * as userActions from '../../actions/users';
import LocalStorage from '../../utils/LocalStorage';
import users from '../../data/users';

const setInitialTeamMembers = () => {
    const localStorageData = LocalStorage.getKeyValue('teamMembers');
    let teamMembers = [];

    if (localStorageData) {
        teamMembers = [...localStorageData];
    } else {
        LocalStorage.setKeyValue('teamMembers', teamMembers);
    }

    return teamMembers;
};

export const initialState = {
    userList: [...users],
    teamMembers: setInitialTeamMembers(),
};

export default function (state = initialState, action) {
    switch (action.type) {
        case userActions.ADD_TEAM_MEMBER: {
            const teamMembers = [...state.teamMembers];
            const { userData } = action;
            teamMembers.push(userData);
            const updatedTeamMembers = _.uniqBy(teamMembers, 'id');
            LocalStorage.setKeyValue('teamMembers', updatedTeamMembers);

            return {
                ...state,
                teamMembers: updatedTeamMembers
            };
        }
        case userActions.REMOVE_TEAM_MEMBER: {
            const { userId } = action;
            const teamMembers = [...state.teamMembers].filter(user => user.id !== userId);
            LocalStorage.setKeyValue('teamMembers', teamMembers);

            return {
                ...state,
                teamMembers
            };
        }
        default:
            return state;
    }
}