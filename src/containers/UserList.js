import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as usersActions from '../actions/users'
import { getTeamMembers } from '../selectors/users';
import './UserList.css';

class UserList extends PureComponent{
    state = {
        showAll: false
    };

    showAll = () => {
        this.setState({
            showAll: true
        });
    };

    removeTeamMember = userId => {
        const { removeTeamMember } = this.props;

        removeTeamMember(userId);
    };

    teamMembersRenderer = teamMembers => {
        return teamMembers.map(user => {
            return <div key={user.id} className='user-row'>
                <div className="user-avatar" onClick={() => {this.removeTeamMember(user.id)}}>
                    <div className="before" />
                    <div className="after" />
                    <div className="tooltip">
                        <span className="tooltip-text">Remove user</span>
                    </div>
                </div>
                <div className="user-info">
                    <span className={user.role === 'External' ? 'external' : ''}>{user.role} member</span>
                    <span>{user.username}</span>
                </div>
            </div>
        });
    };

    render() {
        const { teamMembers } = this.props;
        const { showAll } = this.state;
        const teamMembersSlice = teamMembers.slice(0, 5);

        return (
            <React.Fragment>
            {
                (!showAll) ? (
                    <React.Fragment>
                        { this.teamMembersRenderer(teamMembersSlice) }
                        { teamMembers.length > 5 &&
                            <div className="show-all" onClick={this.showAll}>
                                <span>Show all</span>
                            </div>
                        }
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        { this.teamMembersRenderer(teamMembers) }
                    </React.Fragment>
                )
            }
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    teamMembers: getTeamMembers(state)
});

const mapDispatchToProps = dispatch => ({
    removeTeamMember: userId => {
        dispatch(usersActions.removeTeamMember(userId))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
