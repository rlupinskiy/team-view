export const getUserList = state => state.users.userList;

export const getTeamMembers = state => state.users.teamMembers;