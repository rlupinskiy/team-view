import * as usersActions from './users';

describe('users actions', () => {
    it(`should handle ${usersActions.ADD_TEAM_MEMBER}`, () => {
        const userData = {
            id: 1,
            username: 'Test User',
            role: 'Admin'
        };
        const expectedResult = {
            type: usersActions.ADD_TEAM_MEMBER,
            userData
        };

        expect(usersActions.addTeamMember(userData)).toEqual(expectedResult);
    });

    it(`should handle ${usersActions.REMOVE_TEAM_MEMBER}`, () => {
        const userId = 1;
        const expectedResult = {
            type: usersActions.REMOVE_TEAM_MEMBER,
            userId
        };

        expect(usersActions.removeTeamMember(userId)).toEqual(expectedResult);
    });
});